#!/usr/bin/env bash


#get请求 curl命令默认下就是使用get方式发送http请求。
curl www.baidu.com


# post请求  使用-d参数，形式如下：
curl -d "param1=value1&param2=value2" www.baidu.com

#其他参数  -I:只显示头部信息。 i:显示全部信息。-v:显示解析全过程。-H:添加header信息
data= curl -H "Content-Type:application/json" -H 'Accept-Language: es' -d"{}" http://wf.5veda.net/trustnone/monitorplatform/getRemittancePlans
curl -d "productCode=G21200&startDate=2019-01-22&endDate=2019-01-23" http://127.0.0.1:7778/interface/out/remittanceinfo
echo "---------------------------------"

#post http://127.0.0.1:7778/interface/out/remittanceinfo?productCode=G21200&startDate=2019-01-22&endDate=2019-01-23

-H "Content-Type:application/json" -H 'secretkey: suidifutest'  -H 'merid: suidifu' -H "signedmsg: 5L+d+gq7Oc3MlDzTibOffnJjA/VDeWxGCH9PKH1nnRg9w9jVV4fVdGDRw9YjoqhlLpA9pzxSR4aj4FsMev2FymcRxjMhkeYSh4Sh2gkt76XcVTd9v2u6aDyShwtCA/tcqgScITW+WuXgWrHyUiDsVJAEVtUOOrVqD6AH1M1d/as="
curl -d{"requestId": "asd112","productCode": "HD9900","startDate": "2018-01-23","endDate": "2018-01-25"} http://test-api.yntrust.com/Loan/LoanInfo
"requestId": "asd112","productCode": "HD9900","startDate": "2018-01-23","endDate": "2018-01-25"