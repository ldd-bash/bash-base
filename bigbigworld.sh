#!/usr/bin/env bash
echo `pwd`
pwd
ls -l
# wc -l: 表示统计输出信息的行数
# 文件夹下文件的数量
# grep "^d"表示目录，"^-"表示文件
ls -l|grep "^-"| wc -l
ls -l|grep "^d"| wc -l

ls -l|wc -l
if [ `ls -l| grep "^-"|wc -l` > 5 ]
then
    echo true
else
    echo false
fi