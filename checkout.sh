#!/usr/bin/env bash

      projectArray=(common-sender suidifu-job-core async-api callback-sender monitor-platform remittance-observer subscribe-sender special-account-remittance-consumer business-sender sscc-client recon-repayment-order-source-document-consumer sign citigroup bridgewater-remittance jpmorgan Contra giotto matryoshka swift Xcode morganStanley Barclays SwissRe MunichRe canal-core  Deloitte sun earth gluon wellsfargo Renaissance berkshire coffer zufangbao-springboot-center demo2do-core greenLight PriceWaterHouse  bridgewater-deduct modify-overdue-fee-consumer import-asset-server suidifu-config-server owlman clear-deduct-plan-consumer watchman watchman-message  suidifu-discovery-eureka modify-repayment-plan-consumer recon-repayment-order-deduct-consumer recon-repayment-order-active-consumer system-deduct-repayment-order-scheduler repayment-order-consumer  source-document-reconciliation-consumer tmp-deposit-reconciliation-consumer recon-deduct-consumer pump pump-link-goldman sms-sender suidifu-mybatis-base suidifu-mybatis-shard message-linker message-route)
cd /Users/liudong/workspace/suidifu/zufangbao-springboot-center
for p in ${projectArray[*]} ;

do
	echo '==============================================================================================================================='
	echo -e 'project name:'"\e[1;35m$p\e[0m"
    cd  ../${p}
    git fetch
	git checkout -b $1 $2
	git pull origin $1
done