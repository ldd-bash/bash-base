#!/usr/bin/env bash

#贷前放款数据补漏

#productCodeArray=('I44200' 'I54300' 'I53400' 'I56300' 'I49100' 'I56500' 'I57100' 'I68600' 'I69300' 'I67600' 'I71700' 'I77200' 'I77300' 'I79600' 'I84400' 'I62800' 'J03800' 'J03500' 'J05800' 'J07600')
productCodeArray=('I43400' 'I44200' 'I45700' 'I49100' 'I53400' 'I54300' 'I56300' 'I56500' 'I57000' 'I57100' 'I58600' 'I62800' 'I67600' 'I68600' 'I69300' 'I71700' 'I77200' 'I77300' 'I79600' 'I84400' 'YX1001' 'J03500' 'J03800' 'J05800' 'J07600')
#productCodeArray=('I44200' 'I49100')
count=0
process=0
startDate='2019-08-31' #开始日期
lastDate='2019-02-27' #结束日期
echo "补漏数据时间范围： ${startDate} 到 ${lastDate}"

for i in  ${productCodeArray[*]};do
    subcount=0
    productCode=${i}
    if [ ${productCode} == 'I45700' ]; then
        startDate='2018-08-31'
    elif [ ${productCode} == 'I44200' ]; then
        startDate='2018-09-18'
    elif [ ${productCode} == 'I58600' ]; then
        startDate='2018-10-26'
    elif [[  ${productCode} == 'I54300' ||  ${productCode} == 'I43400' ||  ${productCode} == 'I56300' ||  ${productCode} == 'I49100' ||  ${productCode} == 'I69300' ]]; then
        startDate='2018-11-01'
    elif [ ${productCode} == 'I68600' ]; then
        startDate='2018-12-03'
    else
        startDate='2018-12-21'
    fi
    while [[ ${startDate} < ${lastDate} ]]
    do
        endDate=`date -d "${startDate} + 3 day" +%Y-%m-%d`
        if [[ ${endDate} > ${lastDate}  ]];
        then  endDate=${lastDate}
        fi

#        echo ${startDate}
#        echo ${endDate}
        curl http://127.0.0.1:7778/interface/out/remittanceinfo -d"productCode=${productCode}&startDate=${startDate}&endDate=${endDate}"

        startDate=${endDate}
        count=`expr ${count} + 1`
        subcount=`expr ${subcount} + 1`
    done
    process=`expr ${process} + 1`
    echo "信托代码: ${productCode} 请求次数：${subcount},执行进度：${process}/${#productCodeArray[*]}"
#    sleep 5s
done
echo "信托总数：${#productCodeArray[*]}"
echo "请求总次数： ${count}次"
