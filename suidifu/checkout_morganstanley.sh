#!/usr/bin/env bash

projectArray=(
    demo2do-core
    suidifu-job-core
    swift
    giotto
    coffer
    matryoshka
    gluon
    Renaissance
    berkshire
    coffer
    greenLight
    Barclays
    sun
    owlman
    wellsfargo
    Xcode
    morganStanley
    )

cd /Users/liudong/workspace/suidifu/zufangbao-springboot-center
for p in ${projectArray[*]} ;

do
	echo '==============================================================================================================================='
	echo -e 'project name:'"\e[1;35m$p\e[0m"
    cd  ../${p}
    git fetch
	git checkout $1
	git pull origin $1
done