#!/usr/bin/env bash

#变量、字符串、数组、参数传递



#定义、使用变量
name="hello word"
echo $name
echo ${name}

#只读变量 不能修改值
#readonly name
#name="hello"
echo ${name}

#删除变量 变量被删除后不能再次使用。unset 命令不能删除只读变量。
name="hello kitty"
echo ${name}
unset name
echo ${name}


#字符串 单引号中内容原样输出，双信号中内容会解析输出,单引号需要解析输出如str3
temp=你好
str1='${temp}, I am a teacher'
str2="${temp}, I am a teacher"
echo ${str1}
echo ${str2}
str3=''${temp}', I am a teacher'
echo ${str3}

#字符串长度
echo ${#str1}

#截取字符串 >=1  <3
echo ${str3:1:3}

#查找字符串中字符'I'位置
echo `expr index "$str1" I`




# 数组：
#bash支持一维数组（不支持多维数组），并且没有限定数组的大小。
#类似于 C 语言，数组元素的下标由 0 开始编号。获取数组中的元素要利用下标，下标可以是整数或算术表达式，其值应大于或等于 0。\
#array[@]:数组全部内容   #array[@]数组长度
array1[0]=value0
array1[1]=value1
array1[2]=value2
array=('one' 'two' 'three' 'four' 1 2 3 4)
set array[9]
#for…in:
for i in ${array[@]}
do   echo ${i};
done
#标准、for循环
for((i=0;i<${#array[@]};i++))
do echo ${array[i]}
done
#删除数组指定下标元素
unset array[0]
#修改
array[1]='hello'
for i in ${array[@]}
do   echo ${i};
done








#参数传递：
#执行脚本时给脚本内容传递参数
#我们可以在执行 Shell 脚本时，向脚本传递参数，脚本内获取参数的格式为：$n。
# n 代表一个数字，1 为执行脚本的第一个参数，2 为执行脚本的第二个参数，以此类推……
#   $# 	传递到脚本的参数个数
#   $* 	以一个单字符串显示所有向脚本传递的参数。 如"$*"用「"」括起来的情况、以"$1 $2 … $n"的形式输出所有参数。
#   $$ 	脚本运行的当前进程ID号
#   $! 	后台运行的最后一个进程的ID号
#   $@ 	与$*相同，但是使用时加引号，并在引号中返回每个参数。
#   如"$@"用「"」括起来的情况、以"$1" "$2" … "$n" 的形式输出所有参数。
#   $- 	显示Shell使用的当前选项，与set命令功能相同。
#   $? 	显示最后命令的退出状态。0表示没有错误，其他任何值表明有错误。
echo "执行文件名称：$0"
echo "传递第一个参数：$1"
echo "传递第二个参数：$2"
echo "传递第三个参数：$3"
echo "传递的参数个数：$#"
echo "传递的所有参数：$*"
echo "脚本运行的进程ID：$$"
echo "后台运行的最后一个进程的ID号：$!"
echo "传递的所有参数：$@"
echo "显示最后命令的退出状态 : $?";
#$* 与 $@ 区别：
#    相同点：都是引用所有参数。
#    不同点：只有在双引号中体现出来。假设在脚本运行时写了三个参数 1、2、3，，则 " * " 等价于 "1 2 3"（传递了一个参数），而 "@" 等价于 "1" "2" "3"（传递了三个参数）。







