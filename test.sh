#!/usr/bin/env bash

work_path="/Users/local/Broken";    #修改标红为本地工程绝对路径'


git_checkout()
{
	if [ $2x = x ]; then
		echo "Switch to Witch Brnach?";
		exit 1;
	fi
	cd $work_path/$1;
	git checkout $2
	if [ "$?" = "0" ]; then
		echo -e "Git Checkout $2 \033[32m SUCCESS \033[0m";
	else
		echo -e "Git Checkout $2 \033[31m Failed \033[0m";
	fi
}


git_pull()
{
	if [ $2x = x ]; then
		echo "Pull Witch Brnach?";
		exit 1;
	fi


	cd $work_path/$1;
	cur_branch=`git branch | grep "*" | awk {print $2}`;

	if [ "$cur_branch" = "$2" ]; then
		git pull origin $2:$2 -q
		if [ "$?" = "0" ]; then
			echo -e  "Git Pull $1 on $2: \033[32m SUCCESS \033[0m";
		else
			echo -e "Git Pull $1 on $2: \033[31m FAILED \033[0m";
		fi
		git log -n 1
	else
		echo "Check To Branch $2 first, Current is $cur_branch";
		exit 1;
	fi


}


usage()
{
	echo "Usage ./update_service.sh [-cpg] [branch name]";
}


echo "Current Work Path:$work_path"


if [ $# -lt 1 ]; then
	usage;
	exit -1;
fi


while getopts "c:p:g" arg
do
	case $arg in
		c)
			git_checkout code $OPTARG;    #修改标红文件夹名字'
			git_checkout conf $OPTARG;
			git_checkout common $OPTARG;
			;;
		p)
			git_pull code $OPTARG    #修改标红文件夹名字'
			git_pull common $OPTARG
			git_pull conf $OPTARG
			;;
		g)
			cd $work_path


			if [ -f tags ]; then
				echo "delete old tag file";
				rm tags;
			fi
			echo "generate new tag file";
			;;
		?)
			usage;
			;;
	esac
done