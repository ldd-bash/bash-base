#!/bin/bash

HOSTNAME="localhost"      #数据库信息
PORT="3306"
USERNAME="root"
PASSWORD="123456"

DBNAME="preloan"  #数据库名称

if [ $# -lt 1 ];then
	echo " 请输入文件路径 ！";
    exit
fi

if  [ ! -f $1 ];then
    echo "文件不存在!"
    exit
elif  [ ! -r $1 ];then
    echo "文件权限不可读 !"
    exit
fi

OLD_IFS="$IFS" #保存分隔符
# i is the line number
i=0
failedArray=()
textArray=()
for line in `cat $1`
do
    echo -------当前合同内容为${i}:$line
    IFS="|"
    let "i=$i+1"
    array1=($line)
#
    if [ 2 == ${#array1[@]} ];then
       select_sql="update credit_application credit set credit.attached_content_section=json_insert(credit.attached_content_section,'$.unifiedSocialCreditCode','${array1[1]}','$.businessRegistrationNo','') where credit.merchant_credit_no=(select merchant_credit_no from loan_contract where loan_contract.merchant_contract_no = '${array1[0]}');"
    else
       select_sql="update credit_application credit set credit.attached_content_section=json_insert(credit.attached_content_section,'$.unifiedSocialCreditCode','','$.businessRegistrationNo','${array1[2]}') where credit.merchant_credit_no=(select merchant_credit_no from loan_contract where loan_contract.merchant_contract_no = '${array1[0]}');"
    fi
    echo ${select_sql}
    mysql  ${DBNAME} -e "${select_sql}"

    if [ $? -ne 0 ]; then
        echo "failed:"${array1[0]} >> failed.txt
        failedArray[${#failedArray[@]}]=${array1[0]}
    else
        echo "succeed:"${array1[0]}
    fi
    IFS="$OLD_IFS" # 将IFS恢复成原来的
    echo 执行进度:${i}
done

echo "已执行条数：" ${i}
echo "执行失败数组所有合同:${failedArray[@]}"
if [ 0 -eq ${#failedArray[@]} ]; then
    echo "所有sql执行成功"
else
    echo "有合同未执行成功","失败条数:"${#failedArray[*]}
fi



